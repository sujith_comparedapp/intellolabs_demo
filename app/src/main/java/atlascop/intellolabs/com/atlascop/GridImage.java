package atlascop.intellolabs.com.atlascop;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.content.CursorLoader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.Calendar;

/**
 * Created by ichigo on 22/5/17.
 */
public class GridImage extends Activity {

    int[] imgIds ={
            R.drawable.browse,
            R.drawable.camera

    };
//    public static String URL = "http:// /ingredients.json";
//      public static String URL = " http://192.168.1.9:3000/ingredients.json";

    public static String URL = "http://54.169.107.57/ingredients.json";
    String [] function = {
            "Browse","Take Picture",
    };
    private Button btn_submit;
    private ImageButton back_btn;
    private ImageView imageView,selectted_img;
    ProgressDialog progressDialog;

    static final int PICK_IMAGE_REQUEST = 1;
    static final int CAPTURE_IMAGE = 2;
    public Uri uri ;
    private Calendar cal;
    String filePath;
    private String imgPath;
    File imageStorageDir;
    String TAG = "CHECK";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gridsample);
        cal = Calendar.getInstance();
        isStoragePermissionGranted();
        imageStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "Gehu_Upload");
        if (!imageStorageDir.exists()){
            imageStorageDir.mkdirs();
        }
        ImageAdapter adapter = new ImageAdapter(GridImage.this, function, imgIds);
        GridView gridview = (GridView) findViewById(R.id.gridview);
        gridview.setAdapter(adapter);
        btn_submit = (Button)findViewById(R.id.submit_button);
        selectted_img = (ImageView) findViewById(R.id.selected_img);
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView<?> parent,
                                    View v, int position, long id){
                selectted_img.setImageResource(imgIds[position]);
                if (position == 1){
                    Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    if (intent.resolveActivity(getPackageManager()) != null) {
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, setImageUri());
                        startActivityForResult(intent, CAPTURE_IMAGE);
                    }
                }
                else if(position == 0) {
                    imageBrowse();
                }
            }

        });
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v("CHeck","Button");
                if (filePath != null) { // Browse from gallery
                    Log.v("Check", filePath);
                    image_upload(filePath);
                    // For Browse Image, Image gets selected and then we need to give the submit button

                } else {
//                    Toast.makeText(getApplicationContext(), "Image not selected!", Toast.LENGTH_LONG).show();
                }
            }
        });


    }


    public  boolean isStoragePermissionGranted() {

        if (Build.VERSION.SDK_INT >= 23) {
            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            requestPermissions(new String[]{Manifest.permission.CAMERA},1);
            requestPermissions(new String[]{Manifest.permission.ACCESS_NETWORK_STATE},1);
            requestPermissions(new String[]{Manifest.permission.INTERNET},1);
        }
        return true;
    }

    private Bitmap reduce_image_to_bitmap(String file_path){
        Bitmap bit_map = BitmapFactory.decodeFile(file_path);
        int h = bit_map.getHeight();
        int w = bit_map.getWidth();
        while(h > 500 || w > 500){
            h = h/2;
            w = w/2;
        }
        Bitmap out = Bitmap.createScaledBitmap(bit_map, w, h, false);

        return out;
    }

    private void image_upload(String filePath) {

        Intent intent = new Intent(this, ResultAtlas.class);
        intent.putExtra("IMAGE_PATH", filePath);
        startActivity(intent);
        Log.v("lllll","kick");

    }

    public Uri setImageUri(){

//        File file = new File( imageStorageDir + File.separator + "IMG_" + String.valueOf(System.currentTimeMillis()) + ".jpg");
        Log.v("CamerasetImage",imageStorageDir + File.separator + "IMG_" + String.valueOf(System.currentTimeMillis()) + ".jpg" );
        File file = new File(Environment.getExternalStorageDirectory(), (cal.getTimeInMillis() + ".jpg"));
        Uri imgUri = FileProvider.getUriForFile(this, this.getApplicationContext().getPackageName() + ".provider", file);

//        Uri imgUri = Uri.fromFile(file);
        this.uri = imgUri;
        this.imgPath = file.getAbsolutePath();
        Log.v("URI", String.valueOf(uri));
        Log.v("imgPath",imgPath);
        return imgUri;
    }
    public String getImageUri(){
        return imgPath;
    }

    private void imageBrowse() {

        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        ImageView sel_img = (ImageView) findViewById(R.id.selected_img);
        if (resultCode == RESULT_OK) {
            if(requestCode == PICK_IMAGE_REQUEST){
                //TODO:: Change the size of the image to fit the image view for any type of mobile
                uri = data.getData();
                filePath = getPath(uri);
                Log.v("picUri", String.valueOf(uri));
                Log.v("filePath", filePath);
                Bitmap bimap = reduce_image_to_bitmap(filePath);
                if (bimap != null){
                    selectted_img.setImageBitmap(bimap);
                }
            }
            else if(requestCode == CAPTURE_IMAGE){
                filePath = getImageUri();

                uri = Uri.fromFile(new File(filePath));
                Log.v("CameraURI", String.valueOf(uri));

                if(filePath != null){
                    image_upload(filePath);
                }
            }
            Log.v("Kick", "LL1");

            Log.v("Kick", "LL");
        }
    }


    public class ImageAdapter extends BaseAdapter
    {

        private Context context;
        public ImageAdapter(Context c, String[] web,int[] Imageid)
        {
            context = c;
            imgIds = Imageid;
            function = web;
        }
        @Override
        public int getCount() {
            return imgIds.length;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View grid;
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {

                grid = new View(context);
                grid = inflater.inflate(R.layout.customgrid_layout, null);

                TextView textView = (TextView) grid.findViewById(R.id.title);
                ImageView imageView = (ImageView)grid.findViewById(R.id.thumbnail);
                textView.setText(function[position]);
                imageView.setImageResource(imgIds[position]);
                imageView.setAdjustViewBounds(true);

            } else {
                grid = (View) convertView;
            }
            return grid;

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }


    private boolean checkIfAlreadyhavePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }
    private void requestForSpecificPermission() {
        Log.v("Here", "for permissions");
        ActivityCompat.requestPermissions(this, new String[]{ Manifest.permission.CAMERA, Manifest.permission.GET_ACCOUNTS, Manifest.permission.RECEIVE_SMS, Manifest.permission.READ_SMS, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 101);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 101:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //granted
                } else {
                    //not granted
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    public String getPath(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        Log.v("Check", contentUri.toString());
        CursorLoader loader = new CursorLoader(getApplicationContext(), contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }

}