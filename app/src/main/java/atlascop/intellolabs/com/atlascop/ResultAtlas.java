package atlascop.intellolabs.com.atlascop;

/**
 * Created by ichigo on 22/5/17.
 */
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.SimpleMultiPartRequest;

import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Date;
import java.util.Random;

import pl.droidsonroids.gif.GifImageView;

public class ResultAtlas extends Activity {
    File imageStorageDir;
    private String encodedString ;
    private GifImageView resultImage;
    private ImageView back_btn, selected_result_img;
    private String HIT_URL = "http://54.169.107.57/api/v1/drillbits/";
    private TextView id,diameter_in_mm,diameter_in_inch,product_code,no_of_buttons,diameter_gauge,diameter_center,gauge_buttons_angle,flushing_side,flushing_center,weight,product_no,product_url,thread_in_mm,thread_in_inch,insert_width,insert_height,center_button_angle,retrac,bit_type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        setContentView(R.layout.result);
        String image_path = getIntent().getStringExtra("IMAGE_PATH");
        imageStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "Gehu_Upload");
        if (!imageStorageDir.exists()){
            imageStorageDir.mkdirs();
        }
        Log.v("lllll","kick");
        selected_result_img = (ImageView) findViewById(R.id.selected_result_img);
        Bitmap bimap = reduce_image_to_bitmap(image_path);
        if(bimap != null ){
            selected_result_img.setVisibility(View.VISIBLE);
            selected_result_img.setImageBitmap(reduce_image_to_bitmap(image_path));
        }
        resultImage = (GifImageView) findViewById(R.id.result_img);
        resultImage.setImageResource(R.drawable.loading);
        // TODO:: result image can be used to show the image upload
        if (resultImage != null){
            resultImage.setVisibility(View.VISIBLE);
        }

        back_btn = (ImageView) findViewById(R.id.back_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        image_upload(image_path);
        super.onCreate(savedInstanceState);
    }
    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    private Bitmap reduce_image_to_bitmap(String file_path){
        Bitmap bit_map = BitmapFactory.decodeFile(file_path);
        int h = bit_map.getHeight();
        int w = bit_map.getWidth();
        while(h > 500 || w > 500){
            h = h/2;
            w = w/2;
        }
        Bitmap out = Bitmap.createScaledBitmap(bit_map, w, h, false);

        return out;
    }

    private void image_upload(String image_path){
        Bitmap src=BitmapFactory.decodeFile(image_path);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        src.compress(Bitmap.CompressFormat.JPEG, 60, baos);
        byte[] data = baos.toByteArray();
//        String localHit = "https://upphkyusoa.localtunnel.me/api/v1/drillbits/";
        String encoded = Base64.encodeToString(data, Base64.NO_WRAP);

        Log.v("Request", encoded);


        Log.v("Encoded String",encoded);
        SimpleMultiPartRequest smr = new SimpleMultiPartRequest(Request.Method.POST, HIT_URL,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.v("Responsehere", "HEre");
//                        Toast.makeText(ResultAtlas.this, response, Toast.LENGTH_LONG).show();
                        Log.v("Response", response);
                        resultImage.setVisibility(View.GONE);
                        set_new_response_to_views(response);
//                        set_response_to_views(response, resized_image_path);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                resultImage.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), "Server error. Try again", Toast.LENGTH_LONG).show();
                Log.v("Error here", "Resoponse");
            }

        });

        smr.addStringParam("file_new", encoded);
        smr.setRetryPolicy(new DefaultRetryPolicy(1000 * 60 , DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.v("Request", smr.toString());

        MyApplication.getInstance().addToRequestQueue(smr);

    }

    private void set_new_response_to_views(String response){
        try{
            if(resultImage != null){
                resultImage.setVisibility(View.GONE);
            }
            JSONObject jObj = new JSONObject(response);
            id = (TextView) findViewById(R.id.id);
            diameter_in_mm = (TextView) findViewById(R.id.diameter_in_mm);
            diameter_in_inch = (TextView) findViewById(R.id.diameter_in_inch);
            product_code = (TextView) findViewById(R.id.product_code);
            no_of_buttons = (TextView) findViewById(R.id.no_of_buttons);
            diameter_gauge = (TextView) findViewById(R.id.diameter_gauge);
            diameter_center = (TextView) findViewById(R.id.diameter_center);
            gauge_buttons_angle = (TextView) findViewById(R.id.gauge_buttons_angle);
            flushing_side = (TextView) findViewById(R.id.flushing_side);
            flushing_center = (TextView) findViewById(R.id.flushing_center);
            weight = (TextView) findViewById(R.id.weight);
            product_no = (TextView) findViewById(R.id.product_no);
            product_url = (TextView) findViewById(R.id.product_url);
            thread_in_mm = (TextView) findViewById(R.id.thread_in_mm);
            thread_in_inch = (TextView) findViewById(R.id.thread_in_inch);
            insert_width = (TextView) findViewById(R.id.insert_width);
            insert_height = (TextView) findViewById(R.id.insert_height);
            center_button_angle = (TextView) findViewById(R.id.center_button_angle);
            retrac = (TextView) findViewById(R.id.retrac);
            bit_type = (TextView) findViewById(R.id.bit_type);



            id.setText("id: "+ jObj.getString("id") );
            diameter_in_mm.setText("diameter_in_mm: "+ jObj.getString("diameter_in_mm") );
            diameter_in_inch.setText("diameter_in_inch: "+ jObj.getString("diameter_in_inch") );
            product_code.setText("product_code: "+ jObj.getString("product_code") );
            no_of_buttons.setText("no_of_buttons: "+ jObj.getString("no_of_buttons") );
            diameter_gauge.setText("diameter_gauge: "+ jObj.getString("diameter_gauge") );
            diameter_center.setText("diameter_center: "+ jObj.getString("diameter_center") );
            gauge_buttons_angle.setText("gauge_buttons_angle: "+ jObj.getString("gauge_buttons_angle") );
            flushing_side.setText("flushing_side: "+ jObj.getString("flushing_side") );
            flushing_center.setText("flushing_center: "+ jObj.getString("flushing_center") );
            weight.setText("weight: "+ jObj.getString("weight") );
            product_no.setText("product_no: "+ jObj.getString("product_no") );
            product_url.setText("product_url: "+ jObj.getString("product_url") );
            thread_in_mm.setText("thread_in_mm: "+ jObj.getString("thread_in_mm") );
            thread_in_inch.setText("thread_in_inch: "+ jObj.getString("thread_in_inch") );
            insert_width.setText("insert_width: "+ jObj.getString("insert_width") );
            insert_height.setText("insert_height: "+ jObj.getString("insert_height") );
            center_button_angle.setText("center_button_angle: " + jObj.getString("center_button_angle"));
            retrac.setText("retrac: "+ jObj.getString("retrac"));
            bit_type.setText("bit_type: "+ jObj.getString("bit_type"));
        }
        catch(Exception e){
            e.printStackTrace();
            Toast.makeText(this, "Response error", Toast.LENGTH_LONG).show();
        }

    }

}
